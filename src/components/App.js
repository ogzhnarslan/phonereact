import React, { Component } from 'react';
import logo from '../logo.svg';
import '../App.css';

import Contact from './Contact';

class App extends Component {

	constructor(props) {
		super(props);
		this.addContact = this.addContact.bind(this);
	}

	state = {
		contacts: [{
			name: 'test 1',
			phone: '0554877845'
		}, {
			name: 'test 2',
			phone: '545478787'
		}]
	};

	addContact(contact){
		const { contacts } = this.state;
		contacts.push(contact);
		this.setState({
			contacts
		})
	}

	render() {
		return (
		  <div className="App">
		    <Contact
		    	addContact={this.addContact}
		    	contacts={this.state.contacts} />
		  </div>
		);
	}
}

export default App;